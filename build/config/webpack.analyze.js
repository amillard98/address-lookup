const prod = require('./webpack.prod');
const merge = require('webpack-merge');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer-sunburst')
  .BundleAnalyzerPlugin;

module.exports = merge(prod, {
  plugins: [
    new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      reportFilename: '../stats/bundle_report.html',
      generateStatsFile: true,
      statsFilename: '../stats/bundle_report.json',
    }),
  ],
});
