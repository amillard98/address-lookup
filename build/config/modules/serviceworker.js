const WorkboxWebpackPlugin = require('workbox-webpack-plugin');

module.exports = new WorkboxWebpackPlugin.GenerateSW({
  swDest: 'sw.js',
  clientsClaim: true,
  skipWaiting: true,
});
