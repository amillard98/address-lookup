const common = require('./webpack.common');
const merge = require('webpack-merge');
const UglifyerModule = require('./modules/uglifyer');
const ServiceWorkerModule = require('./modules/serviceworker');

module.exports = merge(common, {
  mode: 'production',
  optimization: {
    minimize: true,
    minimizer: [UglifyerModule],
  },
  plugins: [ServiceWorkerModule],
});
