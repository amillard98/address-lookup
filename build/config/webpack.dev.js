const common = require('./webpack.common');
const merge = require('webpack-merge');
const path = require('path');
const ErrorOverlayPlugin = require('error-overlay-webpack-plugin');
const webpack = require('webpack');
const packageJson = require('../../package.json');
const WebpackDashboardPlugin = require('webpack-dashboard/plugin');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'source-map',
  devServer: resolveDevServer(),
  plugins: [new ErrorOverlayPlugin(), new WebpackDashboardPlugin()],
});

function resolveDevServer() {
  const devServer = {
    compress: false,
    port: 3000,
    overlay: {
      warnings: true,
      errors: true,
    },
  };

  if (packageJson.proxy != null) devServer.proxy = packageJson.proxy;

  return devServer;
}
