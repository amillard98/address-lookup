export interface IRoute {
  path: string;
  component: (props: IRouteComponentProps & any) => JSX.Element;
  exact?: boolean;
  routes?: IRoute[];
}

interface IRouteComponentProps {
  routes?: IRoute[];
}
