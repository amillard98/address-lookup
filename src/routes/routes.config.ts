import { IRoute } from './routes.d';
import Home from '../components/views/home/Home.component';
import NotFound from '../components/views/not-found/NotFound';
import Todo from '../components/views/todo/Todo.component';

const Routes: IRoute[] = [
  {
    path: '/Home',
    exact: true,
    component: Home,
  },
  {
    path: '/Todo',
    exact: true,
    component: Todo,
  },
];

export default Routes;
