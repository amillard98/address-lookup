import * as React from 'react';
import { IRoute } from '../routes';
import { Route } from 'react-router';

function routeWithSubRoutes(route: IRoute) {
  return (
    <Route
      path={route.path}
      exact={true}
      render={props => (
        // pass the sub-routes down to keep nesting
        <route.component {...props} routes={route.routes} />
      )}
    />
  );
}

export default routeWithSubRoutes;
