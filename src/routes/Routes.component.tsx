import * as React from 'react';
import RoutesConfig from './routes.config';
import { Switch, Redirect, Route } from 'react-router';
import RouteWithSubRoutes from './helpers/RouteWithSubRoutes.component';
import NotFound from '../components/views/not-found/NotFound';

function Routes() {
  return (
    <Switch>
      {RoutesConfig.map((route, i) => (
        <RouteWithSubRoutes key={i} {...route} />
      ))}
      <Redirect from="/" to="/Home" />
      <Route component={NotFound} />
    </Switch>
  );
}

export default Routes;
