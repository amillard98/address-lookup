import * as React from 'react';
import { NavLink } from 'react-router-dom';
import './NavBar.scss';
import Dog from '../../../assets/images/dog.jpg';

function NavBar() {
  return (
    <header className="main-header">
      <div className="container">
        <h1 className="">
          <img src={Dog} alt="Logo" width="170" />
        </h1>
        <nav className="main-nav">
          <ul className="main-nav-list">
            <li>
              <NavLink exact to="/Home">
                Home
              </NavLink>
            </li>
            <li>
              <NavLink exact to="/Todo">
                Todo
              </NavLink>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
}

export default NavBar;
