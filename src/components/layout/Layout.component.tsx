import * as React from 'react';
import NavBar from './navbar/NavBar.component';
import Routes from '../../routes/Routes.component';

function Layout() {
  return (
    <>
      <NavBar />
      <main>
        <Routes />
      </main>
    </>
  );
}

export default Layout;
