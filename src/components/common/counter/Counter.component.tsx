import * as React from 'react';
import { ICounterProps } from './counter';

function Counter(props: ICounterProps) {
  const [count, setCount] = React.useState(0);

  return (
    <div>
      <h1>{props.title}</h1>
      <p>Counter: {count}</p>
      <button onClick={increaseCount}>Increase Count</button>
    </div>
  );

  function increaseCount() {
    setCount(count + 1);
  }
}

export default Counter;
