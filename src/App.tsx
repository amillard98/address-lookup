import * as React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Layout from './components/layout/Layout.component';
import './app.scss';

function App() {
  return (
    <BrowserRouter>
      <Layout />
    </BrowserRouter>
  );
}

export default App;
