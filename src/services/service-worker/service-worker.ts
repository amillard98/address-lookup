export function register() {
  if (process.env.NODE_ENV === 'production' && 'serviceWorker' in navigator) {
    window.addEventListener('load', onLoad);
  }
}

async function onLoad() {
  try {
    const registration = await navigator.serviceWorker.register('/sw.js');
    console.log(`SW registered: `, registration);
  } catch (error) {
    console.log(`SW registration failed: `, error);
  }
}
