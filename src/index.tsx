import * as React from 'react';
import * as ReactDOM from 'react-dom';
import 'core-js';
import 'regenerator-runtime/runtime';
import App from './App';
import './configuration/webmanifest/webmanifest';
import * as serviceWorker from './services/service-worker/service-worker';

ReactDOM.render(<App />, document.getElementById('app'));

serviceWorker.register();
